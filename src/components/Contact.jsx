import './contact.css';
import { useState } from 'react';
function Contact (props) {
    const {name, online} = props;
let dotClass = ""
if (props.onLine) {
    dotClass = "onLine";
}
    
    return (

        <div className="Contact">
            <img className='profil-picture' src={props.contactImage} alt=""/>
            <div className={'profil-status '+ dotClass}></div>
            <p className="profil-name"> {props.contactName}</p>
        </div>
    );
}

export default Contact
