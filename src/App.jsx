import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Contact from './components/Contact.jsx'


function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      
        <h1 className="title">Messagerie en ligne</h1>
        <div className='allProfil'>
          <Contact 
            contactName="Michelle Bertrand" 
            contactImage="portraitOne.jpeg"
            onLine={false}
            />
       
          <Contact 
            contactName="Fanny dupons" 
            contactImage="portraitTwo.jpeg"
            onLine={true}
          />
               
          <Contact 
            contactName="Jean Gras" 
            contactImage="portraitThree.jpeg"
            onLine={true}
          />
       
      </div>
    
    </>
  )
}
export default App



